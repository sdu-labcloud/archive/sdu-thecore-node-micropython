import network
import usocket
from time import sleep
import gc
import neopixelFunc
import id
import machine
'''
Starts the AP and waits for a websocket connection to transfer the new wifi credentials
'''


def recover():
    np = neopixelFunc.init()
    neopixelFunc.maintenance(np)

    sta = network.WLAN(network.STA_IF)

    if sta.status() == network.STAT_GOT_IP:
        return 'Already connected'

    ap = network.WLAN(network.AP_IF)

    ap.active(True)
    ap.config(essid='simple-node-' + id.get_mac()[8:])
    ap.config(authmode=3, password='123456789')

    sta.active(True)

    addr = usocket.getaddrinfo('0.0.0.0', 80)[0][-1]

    s = usocket.socket()
    s.bind(addr)
    s.listen(1)

    while not sta.isconnected():
        conn, addr = s.accept()

        ssid = conn.recv(1024)
        ssid = ssid.decode('utf-8')

        pwd = conn.recv(1024)
        pwd = pwd.decode('utf-8')

        conn.close()

        sta.connect(ssid, pwd)

        while sta.status() == network.STAT_CONNECTING:
            neopixelFunc.blink(np, 'blue', 2)
        if sta.status() == network.STAT_GOT_IP:
            break

    ap.active(False)
