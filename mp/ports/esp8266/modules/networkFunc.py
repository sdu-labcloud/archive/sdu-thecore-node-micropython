import network
import socket
import urequests
import ujson

token = None


def createNode():
    import id
    serial, mac, os = id.get_all()
    json = ujson.dumps(
        {'serial_number': serial, 'mac_address': mac, 'operating_system': os})
    response = urequests.post('https://api.thecore.sdu.dk/v1/nodes',
                              headers={'Content-Type': 'application/json'},
                              data=json)
    if response.status_code == 201:
        return response.json()['data']['id']
    if response.status_code == 409:
        return response.json()['id']
    return None


def createSession(nodeId):
    response = urequests.post(
        'https://api.thecore.sdu.dk/v1/nodes/'+nodeId+'/sessions')
    if response.status_code == 201:
        global token
        token = response.json()['data']['token']
        return response.json()['data']['id']
    return None


def checkSession(sessionId, nodeId):
    response = urequests.get(
        'https://api.thecore.sdu.dk/v1/nodes/'+nodeId+'/sessions/'+sessionId)
    if response.status_code == 200:
        return response.json()['data']['authorized'], response.json()['data']['identify']
    return None, None


def checkIdentify(sessionId, nodeId):
    response = urequests.get(
        'https://api.thecore.sdu.dk/v1/nodes/'+nodeId+'/sessions/'+sessionId)
    if response.status_code == 200:
        return response.json()['data']['identify']
    return None


def getMachine(nodeId):
    response = urequests.get(
        'https://api.thecore.sdu.dk/v1/nodes/'+nodeId)
    if response.status_code == 200:
        return response.json()['data']['machine']
    return None


def getProperties(machineId):
    response = urequests.get(
        'https://api.thecore.sdu.dk/v1/machines/'+machineId+'?populate=permission')
    if response.status_code == 200:
        return response.json()['data']['permission']['scheduling'], response.json()['data']['permission']['workflow'], response.json()['data']['permission']['id']
    return None


def setStatus(machineId, status):
    global token
    json = ujson.dumps({'status': status})
    response = urequests.patch('https://api.thecore.sdu.dk/v1/machines/'+machineId,
                               headers={'Content-Type': 'application/json',
                                        'Authorization': 'JWT '+token},
                               data=json)
    return response.status_code == 200


def checkAccess(tagId, permissionId):
    global token
    response = urequests.get(
        'https://api.thecore.sdu.dk/v1/users/'+str(tagId),
        headers={'Authorization': 'JWT '+token})
    if response.status_code == 200:
        return permissionId in response.json()['data']['permissions']
    return False


def setTime():
    pass
    # TODO get current time from server
