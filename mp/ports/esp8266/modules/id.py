import esp
import machine
import network

'''
    Helper functions to provide information about the node
'''


def get_serial():
    return hex(int.from_bytes(machine.unique_id(), 'big'))[2:10].upper()


def get_mac():
    wlan = network.WLAN()
    return hex(int.from_bytes(wlan.config('mac'), 'big'))[2:14].upper()


def get_os():
    return 'micropython'


def get_all():
    '''
    Returns all ID values in the order of:
    Serial
    Mac
    OS
    '''
    serial = get_serial()
    mac = get_mac()
    os = get_os()

    return serial, mac, os
