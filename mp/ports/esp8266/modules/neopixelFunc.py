import machine
import neopixel
import time

# Colors are represented in RGB (Red, Green, Blue)
# From 0 to 255
colors = {
    'WHITE': [255, 255, 255],
    'BLACK': [0, 0, 0],
    'RED': [255, 0, 0],
    'GREEN': [0, 255, 0],
    'BLUE': [0, 0, 255],
    'YELLOW': [255, 255, 0],
    'ORANGE': [255, 50, 0],
    'VIOLET': [255, 0, 255]
}


def init():
    np = neopixel.NeoPixel(machine.Pin(14), 1)
    return np


def setColor(np, color):
    try:
        color = colors[color.upper()]
    except KeyError:
        color = [0, 0, 0]

    np[0] = (color[0], color[1], color[2])
    np.write()


def blink(np, color, count):
    """
    Parameters

    """
    for i in range(0, count):
        setColor(np, color)
        time.sleep(0.1)
        setColor(np, 'black')
        time.sleep(0.1)


def fade(np, color1, color2, t):
    """ Fades between color1 and color 2

    Arguments:
        np {NeoPixel} -- The NeoPixel object
        color1 {string} -- The initial color
        color2 {string} -- The final color
        t {float} -- Time between each division
    """
    try:
        color1rgb = colors[color1.upper()]
    except KeyError:
        color1rgb = [0, 0, 0]

    try:
        color2rgb = colors[color2.upper()]
    except KeyError:
        color2rgb = [0, 0, 0]

    while color1rgb != color2rgb:
        for i in range(0, 3):
            if color1rgb[i] < color2rgb[i]:
                color1rgb[i] += 1
            if color1rgb[i] > color2rgb[i]:
                color1rgb[i] -= 1
        np[0] = (color1rgb[0], color1rgb[1], color1rgb[2])
        np.write()
        time.sleep(t)


def approved(np):
    setColor(np, 'green')


def denied(np):
    blink(np, 'red', 3)
    setColor(np, 'red')


def maintenance(np):
    setColor(np, 'orange')


def identify(np):
    blink(np, 'orange', 5)


def off(np):
    setColor(np, 'black')
