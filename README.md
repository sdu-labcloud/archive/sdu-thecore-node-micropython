[![pipeline status](https://gitlab.com/Clapfire/simple-node/badges/master/pipeline.svg)](https://gitlab.com/Clapfire/simple-node/commits/master)
# Simple Node for The Core at SDU Sønderborg
## Making changes
To make changes to the firmware, add any edited files to this repository. The changes will be integrated by the build process. \
Initial files can be found in the [Micropython repository](https://github.com/micropython/micropython). Since this project uses an ESP8266, use the files from the ESP8266 port.

## Frozen modules
Frozen modules are compiled into byte-code or native code in order to save memory. To add frozen modules, simply place the python files into the Modules folder. \
Python packages can be added in this way. Make a directory for each package and put an \_\_init__.py file, as well as other source files inside.

## Initial deployment
Firmware is uploaded to the ESP8266 using [esptool from espressif](https://github.com/espressif/esptool). 
Install the tool using `pip install esptool`. \
To flash the Sonoff Basic, pins must be soldered unto the programming ports. \
Use an USB - UART converter to connect to the ESP8266. 
Make sure to use 3.3V, as 5V will destroy the chip. \
In order to enter flash mode, the button must be held down when power is applied. \
Execute `esptool erase_flash` to erase the flash, then \
`esptool write_flash --flash_mode dio 0x0 yaota8266.bin 0x3c000 firmware-ota.bin`

## OTA deployment
To perform an OTA update to the firmware, [yaota8266](https://github.com/pfalcon/yaota8266) is used. In the ota-client folder, execute: \
`ota_client.py live-ota <path> -a <ip>` \
_Note that this is a Python 3 script._ \
Replace `<path>` with the path to the firmware, and `<ip>` with ip of the node.
## Sonoff Basic Pinout
![Pinout of the Sonoff Basic](https://camo.githubusercontent.com/0a599aedf8072580f5c46cbfddbbf0eb7a861386/687474703a2f2f74696e6b65726d616e2e6361742f77702d636f6e74656e742f75706c6f6164732f323031362f30362f70696e6f75745f66726f6e74782e6a7067) \
More information can be found at the [Tasmota Wiki](https://github.com/arendst/Sonoff-Tasmota/wiki/GPIO-Locations) \
__TODO__ add instructions on how to initiate OTA boot.

## About Card ID's
Card ID's are read as little endian, with the card type attached. The card type (04) should be discarded. The remaining 4 bytes are read as LSB. For example, the card reader may return 04 6B 65 E8 2D. 04 would be discarded, and the value should be read as 2D E8 65 6B. This is then converted to an integer, ie: ‭770205035‬